<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Department;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $soict = Department::create([
            'name' => 'School of information & Communications Technology.',
            'abrevation' => 'SOICT',
        ]);

        $sote = Department::create([
            'name' => 'School of Teacher Education.',
            'abrevation' => 'SOTE',
        ]);

        $soit = Department::create([
            'name' => 'School of Industrial Technology.',
            'abrevation' => 'SOIT',
        ]);

        $user = User::create([
            'first_name' => 'Super', 
            'last_name' => 'Admin', 
            'middle_name' => '', 
            'department' => 'Admintration',
            'role' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('secret')
        ]);
    
        $role = Role::create(['name' => 'Admin']);
     
        $permissions = Permission::pluck('id','id')->all();
   
        $role->syncPermissions($permissions);
     
        $user->assignRole([$role->id]);

        /* ***************** */
        $employee = Role::create(['name' => 'Faculty']);
     
        // $employee_permissions = Permission::pluck('id','id')->all();
        // $employee->syncPermissions($employee_permissions);
     
        // $user->assignRole([$employee->id]);
    }
}
