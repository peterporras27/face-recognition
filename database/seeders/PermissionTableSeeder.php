<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
 
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [

           'user-list',
           'user-create',
           'user-edit',
           'user-delete',  
             
           'role-list',
           'role-create',
           'role-edit',
           'role-delete',           

           'log-list',
           'log-create',
           'log-edit',
           'log-delete',

           'absent-list',
           'absent-create',
           'absent-edit',
           'absent-delete',

           'undertime-list',
           'undertime-create',
           'undertime-edit',
           'undertime-delete',

           'overtime-list',
           'overtime-create',
           'overtime-edit',
           'overtime-delete',

           'report-list',
           'report-create',
           'report-edit',
           'report-delete',
        ];
     
        foreach ($permissions as $permission) {
             Permission::create(['name' => $permission]);
        }
    }
}
