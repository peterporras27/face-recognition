<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImagesToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('face_up')->nullable();
            $table->string('face_down')->nullable();
            $table->string('face_left')->nullable();
            $table->string('face_right')->nullable();
            $table->string('lean_left')->nullable();
            $table->string('lean_right')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'face_up',
                'face_down',
                'face_left',
                'face_right',
                'lean_left',
                'lean_right',
            ]);
        });
    }
}
