<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PublicController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DepartmentController;

use App\Http\Controllers\LogsController;
use App\Http\Controllers\AbsencesController;
use App\Http\Controllers\UnderTimeController;
use App\Http\Controllers\OverTimeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'index'])->name('home');
Route::get('/scan/{id}', [PublicController::class, 'scanner'])->name('scanner');

Route::get('/autofill', [PublicController::class, 'autofill'])->name('autofill');

Route::get('/time-in', [PublicController::class, 'timein'])->name('timein');
Route::get('/time-out', [PublicController::class, 'timeout'])->name('timeout');
Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');
Route::get('/log', [PublicController::class, 'log'])->name('log');
Route::get('/clear', [PublicController::class, 'clear'])->name('clear');

Route::post('/leave/{id}', [UserController::class, 'leave'])->name('leave');
Route::delete('/leave/{id}/delete', [UserController::class, 'leave_delete'])->name('leave-delete');

require __DIR__.'/auth.php';

Auth::routes([
  'register' => false, // Registration Routes...
]);

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('departments', DepartmentController::class);

    Route::resource('logs', LogsController::class);
    Route::resource('absences', AbsencesController::class);
    Route::resource('undertime', UnderTimeController::class);
    Route::resource('overtime', OverTimeController::class);
});