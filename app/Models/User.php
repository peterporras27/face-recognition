<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Carbon;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'department',
        'status',
        'email',
        'role',
        'profile_image',
        'password',
        'face_up',
        'face_down',
        'face_left',
        'face_right',
        'lean_left',
        'lean_right',
        'department_id',
        'rate'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getImageAttribute()
    {
       return $this->profile_image;
    }

    public function timeIn()
    {
        return Log::where([
            ['user_id', '=', $this->id],
            ['login', '=', true]
        ])->whereDate('created_at', Carbon::today())->first();
    }

    public function timeOut()
    {
        return Log::where([
            ['user_id', '=', $this->id],
            ['logout', '=', true]
        ])->whereDate('created_at', Carbon::today())->latest()->first();
    }

    public function isAbsent()
    {
        $haslog = Log::where([
            ['user_id', '=', $this->id],
            ['login', '=', true]
        ])->whereDate('created_at', Carbon::today())->first();

        return ($haslog) ? false : true;
    }

    public function loginAgo($min = 5)
    {
        $haslog = Log::where([
            ['user_id', '=', $this->id],
            ['login', '=', true],
            ['created_at', '<', Carbon::now()->subMinutes($min)->toDateTimeString()]
        ])->first();

        return ($haslog) ? true : false;
    }

    public function logoutAgo($min = 5)
    {
        $haslog = Log::where([
            ['user_id', '=', $this->id],
            ['logout', '=', true],
            ['created_at', '<', Carbon::now()->subMinutes($min)->toDateTimeString()]
        ])->first();

        return ($haslog) ? true : false;
    }

    public function getTimeInOut($from, $to){

        $in = Log::where([
            ['user_id', '=', $this->id],
            ['login', '=', true]
        ])->whereBetween('created_at', [$from, $to])->first();

        $out = Log::where([
            ['user_id', '=', $this->id],
            ['logout', '=', true]
        ])->whereBetween('created_at', [$from, $to])->latest()->first();

        $time_in = '';
        $time_out = '';
        
        $time['undertime'] = [
            'hours' => '',
            'minutes' => '',
            'seconds' => ''
        ];

        if ( $in && $out ) {

            $startTime = Carbon::parse($in->created_at);
            $endTime = Carbon::parse($out->created_at);

            $hours = (int) $startTime->diff($endTime)->format('%H');
            $minutes = (int) $startTime->diff($endTime)->format('%I');
            $seconds = (int) $startTime->diff($endTime)->format('%S');

            // Overtime hours
            if ( $hours >= 4 && $seconds > 0 ) {

                // overtime hours

            } else {

                // compute undertime hours
                $h = 4-$hours;

                $time['undertime']['hours'] = $h;
                $time['undertime']['minutes'] = $minutes;
                $time['undertime']['seconds'] = $seconds;
            }
        }

        if ( $in ) { $time_in = $in->created_at->format('h:i:s a'); }
        if ( $out ) { $time_out = $out->created_at->format('h:i:s a'); }

        $time['in'] = $time_in;
        $time['out'] = $time_out;

        return $time;
    }

    public function timeDiff($date)
    {
        $date = Carbon::parse($date);

        $login = Log::where([
            ['user_id', '=', $this->id],
            ['login', '=', true]
        ])->whereDate('created_at', $date)->first();

        $logout = Log::where([
            ['user_id', '=', $this->id],
            ['logout', '=', true]
        ])->whereDate('created_at', $date)->latest()->first();

        $am_from =  $date->format('Y-m-d').' 00:00:01';
        $am_to =  $date->format('Y-m-d').' 11:59:59';

        $pm_from =  $date->format('Y-m-d').' 12:00:01';
        $pm_to =  $date->format('Y-m-d').' 23:59:59';

        $am_undertime = $this->getTimeInOut($am_from, $am_to);
        $pm_undertime = $this->getTimeInOut($pm_from, $pm_to);
        $utime = $this->getUndertimes($am_undertime['undertime'],$pm_undertime['undertime']);

        $time = [
            'am' => $am_undertime,
            'pm' => $pm_undertime,
            'overtime' => array('hours' => 0,'minutes' => 0,'seconds' => 0),
            'undertime' => array(
                'hours' => $utime['hours'],
                'minutes' => $utime['minutes'],
                'seconds' => $utime['seconds']
            ),
            'consumed' => array('hours' => 0,'minutes' => 0,'seconds' => 0),
            'utime' => $utime['utime'],
            'is_utime' => $utime['is_undertime']
        ];

        if ( $login && $logout ) 
        {
            $startTime = Carbon::parse($login->created_at);
            $endTime = Carbon::parse($logout->created_at);

            $time['consumed']['hours'] = (int) $startTime->diff($endTime)->format('%H');
            $time['consumed']['minutes'] = (int) $startTime->diff($endTime)->format('%I');
            $time['consumed']['seconds'] = (int) $startTime->diff($endTime)->format('%S');

            // calculate undertime and overtime.
            $hours = (int) $time['consumed']['hours'];
            $minutes = (int) $time['consumed']['minutes'];
            $seconds = (int) $time['consumed']['seconds'];

            // Overtime hours
            if ( $hours >= 8 && $seconds > 0 ) {

                // compute overtime hours
                $h = $hours-8;

                $time['overtime']['hours'] = $h;
                $time['overtime']['minutes'] = $minutes;
                $time['overtime']['seconds'] = $seconds;

            } else {

                // compute undertime hours
                $h = 8-$hours;

                // $time['undertime']['hours'] = $h;
                // $time['undertime']['minutes'] = $minutes;
                // $time['undertime']['seconds'] = $seconds;
            }
        }

        return $time;
    }

    public function noAbsences($from, $to)
    {
        $absences = 0;
        $return = [
            'absences' => 0,
            'ut_count' => 0,
            'ut_dates' => [],
            'ab_dates' => [],
        ];

        if ($from && $to) {
            
            $from_month = date('m', strtotime($from));
            $from_year = date('Y', strtotime($from));
            $from_day = date('d', strtotime($from));
            
            $to_month = date('m', strtotime($to));
            $to_year = date('Y', strtotime($to));
            $to_day = date('d', strtotime($to));

            $numdays = cal_days_in_month(CAL_GREGORIAN, $from_month, $from_year);

            $days = [];

            for( $d=1; $d <= $numdays; $d++ )
            {
                $time = mktime(12, 0, 0, $from_month, $d, $from_year);
                if (date('m', $time)==$from_month) 
                {   
                    if ( !in_array(date('D', $time), ['Sat','Sun']) )  {
                        $days[date('Y-m-d', $time)] = true;
                    }
                }
            }

            $to_numdays = cal_days_in_month(CAL_GREGORIAN, $to_month, $to_year);
            for( $d=1; $d <= $to_numdays; $d++ )
            {
                $time = mktime(12, 0, 0, $to_month, $d, $to_year);
                if (date('m', $time)==$to_month) 
                {   
                    if ( !in_array(date('D', $time), ['Sat','Sun']) )  {
                        $days[date('Y-m-d', $time)] = true;
                    }
                }
            }

            $logs = Log::where([
                ['user_id', '=', $this->id]
            ])->whereBetween('created_at', [$from, $to])->get();

            foreach ($logs as $log) {
                $days[$log->created_at->format('Y-m-d')] = false;
            }

            $hms = [
                'hours' => 0,
                'minutes' => 0,
                'seconds' => 0,
            ];

            foreach ($days as $day => $isAbsent ) 
            {
                $utime = $this->timeDiff($day);
                $d = explode('-', $day);
                $ut = $utime['undertime'];

                if ($isAbsent) 
                {
                    $return['absences']++;
                    $return['ab_dates'][] = $d[2];
                }

                if ( $utime['is_utime'] ) 
                {
                    $return['ut_count']++;
                    
                    if (!in_array($d[2], $return['ab_dates'])) {
                        $return['ut_dates'][] = $d[2];
                    }

                    $h = $ut['hours'];
                    $m = $ut['minutes'];
                    $s = $ut['seconds'];

                    if ($s >= 60) {
                        $m = $m+1;
                        $s = $s-60;
                    }

                    if ($m >= 60) {
                        $h = $h+1;
                        $m = $m-60;
                    }

                    $s = $hms['seconds']+$s;
                    $m = $hms['minutes']+$m;
                    $h = $hms['hours']+$h;

                    if ($s >= 60) {
                        $m = $m+1;
                        $s = $s-60;
                    }

                    if ($m >= 60) {
                        $h = $h+1;
                        $m = $m-60;
                    }

                    $hms['hours'] = $h;
                    $hms['minutes'] = $m;
                    $hms['seconds'] = $s;
                }
            }

            $return['utime'] = $hms;
        }

        return $return;
    }

    public function getUndertimes($am,$pm)
    {
        $amh = (empty($am['hours'])) ? '': $am['hours']; 
        $amm = (empty($am['minutes'])) ? '': $am['minutes']; 
        $ams = (empty($am['seconds'])) ? '': $am['seconds'];

        $pmh = (empty($pm['hours'])) ? '': $pm['hours']; 
        $pmm = (empty($pm['minutes'])) ? '': $pm['minutes']; 
        $pms = (empty($pm['seconds'])) ? '': $pm['seconds'];

        $unth = 0;
        
        $amh = preg_replace('/\D/', '', $amh);
        if( !empty($amh) ) { $unth = $amh; $amh = $amh.':'; } 
        
        $pmh = preg_replace('/\D/', '', $pmh);
        if( !empty($pmh) ) { $unth = $unth+$pmh;$pmh = $pmh.':';  }

        $untm = 0;
        
        $amm = preg_replace('/\D/', '', $amm);
        if( !empty($amm) ) { $untm = $amm; $amm = $amm.':'; } 
        
        $pmm = preg_replace('/\D/', '', $pmm);
        if( !empty($pmm) ) { $untm = $untm+$pmm;$pmm = $pmm.':';  }

        $unts = 0;
        
        $ams = preg_replace('/\D/', '', $ams);
        if( !empty($ams) ) { $unts = $ams; $ams = $ams; } 
        
        $pms = preg_replace('/\D/', '', $pms);
        if( !empty($pms) ) { $unts = $unts+$pms;$pms = $pms;  }

        $undertime = false;
        $hr = 0;
        $min = 0;
        $sec = 0;

        if ($unts) 
        { 
            if ( $unts > 60 ) 
            {
                $unts = $unts-60;
                $untm = $untm+1;
            }

            $unts = $unts;
            $sec = $unts;
            $min = $untm;
            $undertime = true;

        } else {

            $unts = '';
        }

        if ($untm) 
        {
            if ( $untm > 60 ) 
            {
                $untm = $untm-60;
                $unth = $unth+1;
            }

            $hr = $unth;
            $min = $untm;
            $undertime = true;

            $untm = $untm.':'; 

        } else {

            $untm = '';
        }

        if ($unth) 
        { 
            $unth = $unth.':'; 

        } else {

            $unth = '';
        }

        return array(
            'is_undertime' => $undertime,
            'utime' => $unth.$untm.$unts,
            'hours' => $hr,
            'minutes' => $min,
            'seconds' => $sec,
        );
    }
}
