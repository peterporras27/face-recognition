<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UnderTime extends Model
{
    use HasFactory;

    //assign primary key field name.
    protected $primaryKey = 'id';
    //database table name.
    protected $table = 'undertimes';

    protected $fillable = [
        'time',
        'user_id',
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User','user_id');
    }
}
