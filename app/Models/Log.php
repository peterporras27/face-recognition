<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    //assign primary key field name.
    protected $primaryKey = 'id';
    //database table name.
    protected $table = 'logs';

    protected $fillable = [
        'login',
        'logout',
        'user_id',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function overtime(){
        
    }

}
