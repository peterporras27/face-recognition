<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $roles = Role::all();

        $users = User::orderBy('last_name','DESC')->whereHas('roles', function($q){
            $q->where('name', 'Faculty');
        })->paginate(12);

        return view('dashboard', compact('users','roles'));
    }
}
