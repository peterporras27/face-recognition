<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;

class DepartmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Department::orderBy('id','DESC')->paginate(5);
        return view('department.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $params = [
            'name' => 'required',
            'abrevation' => 'required'
        ];

        $this->validate($request, $params);

        $department = Department::create($request->all());
    
        return redirect()->route('departments.index')
                        ->with('success','Department created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::find($id);

        if ( !$department ) {
            return redirect()->route('departments.index')
                ->with('error','Department no longer exists.');
        }

        return view('department.edit',compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $department = Department::find($id);

        $params = [
            'name' => 'required',
            'abrevation' => 'required'
        ];

        $this->validate($request, $params);

        if (!$department) {
            return redirect()->route('departments.index')
                        ->with('error','Department no longer exists.');
        }

        $department->fill($request->all());
        $department->save();
    
        return redirect()->route('departments.edit', $department->id)
                        ->with('success','Department successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dep = Department::find($id);

        if ($dep) {
            $dep->delete();
            return redirect()->route('departments.index')
                ->with('success','Department deleted successfully');
        }

        return redirect()->route('departments.index')
                ->with('error','Department no longer exist.');
    }
}
