<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Department;
use App\Models\Absent;
use App\Models\Log;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Arr;
use App\Traits\UploadTrait;
use Illuminate\Support\Str;
use DateTime;
use DateInterval;
use DatePeriod;

use Hash;
use DB;

class UserController extends Controller
{
    use UploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index','show']]);
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    public function leave(Request $request, $id)
    {
        $params = [
            'from' => 'required|date',
            'to' => 'required|date',
            'cause' => 'required'
        ];

        $this->validate($request, $params);

        $input = $request->all();

        $begin = new DateTime($input['from']);
        $end = new DateTime($input['to']);

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        foreach ($period as $dt) {

            $leave = Absent::where('created_at', '=', $dt->format('Y-m-d') . ' 00:00:00')->first();

            if (!$leave) {
                $leave = new Absent();
            }

            $leave->cause = $input['cause'];
            $leave->reason = $input['reason'];
            $leave->absent = false;
            $leave->user_id = $id;
            $leave->created_at = $dt->format('Y-m-d').' 00:00:00';
            $leave->save();
        }
        
        return redirect()->route('users.show',$id)
            ->with('success','Leave successfully updated.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->paginate(5);
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        $departments = Department::all();
        return view('users.create',compact('roles','departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = [
            'first_name' => 'required',
            'last_name' => 'required',
            'middle_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required',
            'rate' => 'required'
        ];

        $imagesarr = ['profile_image','face_up','face_down','face_left','face_right','lean_left','lean_right'];

        foreach ($imagesarr as $arr) {
            if ( $request->has($arr) ) 
            {
                if (!is_null($request->file($arr))) 
                {
                    $params[$arr] = 'required|image|mimes:jpeg,png,jpg,gif|max:2048';
                }
            }
        }

        $this->validate($request, $params);
    
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        if (in_array('Admin', $request->input('roles'))) {
            $input['role'] = 'admin';
        }

        foreach ($imagesarr as $img) {
            // Check if a profile image has been uploaded
            if ($request->has($img)) {
                // Get image file
                $image = $request->file($img);
                // Make a image name based on user name and current timestamp
                $name = Str::slug($request->input('first_name').'_'.$request->input('last_name').'_'.$img);
                // Define folder path
                $folder = '/uploads/images/';
                // Make a file path where image will be stored [ folder path + file name + file extension]
                $filePath = $name. '.' . $image->getClientOriginalExtension();
                // Upload image
                $this->uploadOne($image, $folder, 'public', $name);
                // Set user profile image path in database to filePath
                $input[$img] = $filePath;
            }
        }

        $user = User::create($input);

        $user->assignRole($request->input('roles'));
    
        return redirect()->route('users.index')
                        ->with('success','User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = User::find($id);
        $logs = Log::where('user_id','=',$id)->orderBy('created_at','DESC');

        $month = $request->input('month');
        $days = [];
        $numdays = '';
        $wage = '0.00';

        if ( $request->input('month') ) {

            $logs = $logs->whereMonth('created_at', $request->input('month'))->get();

            $year = date('Y');
            $numdays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

            for( $d=1; $d <= $numdays; $d++ )
            {
                $time = mktime(12, 0, 0, $month, $d, $year);
                if (date('m', $time)==$month) {
                    $days[date('m/d/Y D', $time)] = array(
                        'am' => array('in' => '','out' => ''),
                        'pm' => array('in' => '','out' => ''),
                        'undertime' => ['hours'=>'','minutes'=>'','seconds'=>''],
                        'overtime' => ['hours'=>'','minutes'=>'','seconds'=>''],
                        'consumed' => ['hours'=>'','minutes'=>'','seconds'=>'']
                    );
                }
            }

            foreach ($logs as $log) {
                $days[$log->created_at->format('m/d/Y D')] = $log->user->timeDiff($log->created_at);   
            }

            // dd($days);

        } else if ($request->input('date')) {

            $logs = $logs->whereDate('created_at', $request->input('date'))->paginate(20);

        } else {

            $logs = $logs->paginate(15);
        }

        $months = [
            '01' => 'January',
            '02' => 'Febuary',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December'
        ];

        $leaves = Absent::where('user_id','=',$id)->where(function ($q) {
            $q->where('cause', 'sick-leave')
                ->orWhere('cause', 'vacation-leave')
                ->orWhere('cause', 'other');
        })->paginate();

        return view('users.show',compact('user','logs','months','month','days','numdays','leaves','wage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();
        $departments = Department::all();

        return view('users.edit',compact('user','roles','userRole','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $params = [
            'first_name' => 'required',
            'last_name' => 'required',
            'middle_name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required',
            'rate' => 'required'
        ];

        $imagesarr = ['profile_image','face_up','face_down','face_left','face_right','lean_left','lean_right'];

        foreach ($imagesarr as $arr) {
            if ( $request->has($arr) ) 
            {
                if (!is_null($request->file($arr))) 
                {
                    $params[$arr] = 'required|image|mimes:jpeg,png,jpg,gif|max:2048';
                }
            }
        }

        $this->validate($request, $params);
    
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));    
        }
    
        $user = User::find($id);
        
        foreach ($imagesarr as $img) {
            // Check if a profile image has been uploaded
            if ( $request->has($img) ) 
            {
                if ( !is_null($request->file($img)) ) 
                {
                    // Get image file
                    $image = $request->file($img);
                    // Make a image name based on user name and current timestamp
                    $name = Str::slug($request->input('first_name').'_'.$request->input('last_name').'_'.$img);
                    // Define folder path
                    $folder = '/uploads/images/';
                    // Make a file path where image will be stored [ folder path + file name + file extension]
                    $filePath = $name. '.' . $image->getClientOriginalExtension();

                    // Upload image
                    $this->uploadOne($image, $folder, 'public', $name);
                    // Set user profile image path in database to filePath

                    // $user->profile_image = $filePath;
                    if (!empty($user->$img)) {
                        //$this->deleteOne($folder, 'public', $user->$img);    
                    }

                    $input[$img] = $filePath;
                }
            }
        }

        $user->update($input);

        DB::table('model_has_roles')->where('model_id',$id)->delete();
    
        $user->assignRole($request->input('roles'));
    
        return redirect()->route('users.edit',$id)
                        ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (!empty($user->profile_image)) {
            $this->deleteOne('/uploads/images/', 'public', $user->profile_image);    
        }

        if (!empty($user->face_up)) {
            $this->deleteOne('/uploads/images/', 'public', $user->face_up);    
        }

        if (!empty($user->face_down)) {
            $this->deleteOne('/uploads/images/', 'public', $user->face_down);    
        }

        if (!empty($user->face_left)) {
            $this->deleteOne('/uploads/images/', 'public', $user->face_left);    
        }

        if (!empty($user->face_right)) {
            $this->deleteOne('/uploads/images/', 'public', $user->face_right);    
        }

        if (!empty($user->lean_left)) {
            $this->deleteOne('/uploads/images/', 'public', $user->lean_left);    
        }

        if (!empty($user->lean_right)) {
            $this->deleteOne('/uploads/images/', 'public', $user->lean_right);    
        }

        $logs = Log::where('user_id','=',$id)->get();
        if($logs){ foreach ($logs as $log) { $log->delete(); } }

        User::find($id)->delete();

        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }

    public function leave_delete(Request $request, $id)
    {
        Absent::find($id)->delete();

        return redirect()->route('users.show',$request->input('user_id'))
            ->with('success','Leave successfully removed.');
    }
}
