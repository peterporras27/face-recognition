<?php

namespace App\Http\Controllers;

use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Department;
use App\Models\Absent;
use App\Models\User;
use App\Models\Log;


class PublicController extends Controller
{
    public function index()
    {
        $departments = Department::all();
        return view('welcome',compact('departments'));
    }

    public function scanner($id)
    {
        $faculty = User::orderBy('last_name','DESC')->whereHas('roles', function($q) use ($id) {
            $q->where('name', 'Faculty');
            $q->where('department_id', $id);
        })->get();

        if ($faculty) {
            // create absences for all faculty
            foreach ($faculty as $fac) 
            {
                // check if record exist
                $hasRecord = Absent::where('user_id','=',$fac->id)->whereDate('created_at', Carbon::today())->first();
                // create record if record does not exist
                if ( !$hasRecord ) 
                {
                    $absent = new Absent;
                    $absent->absent = ($fac->isAbsent()) ? true : false;
                    $absent->user_id = $fac->id;
                    $absent->save();
                }
            }
        }

        $users = User::orderBy('last_name','DESC')->whereHas('roles', function($q) use ($id) {
            $q->where('name', 'Faculty');
            $q->where('department_id', $id);
        })->get(['id','profile_image','first_name','last_name','middle_name','face_up','face_down','face_left','face_right','lean_left','lean_right'])->toArray();

        $logs = [];
        $uids_in = [];
        $uids_out = [];

        $records = Log::whereDate('created_at', Carbon::today())->orderBy('created_at','desc')->get();

        if ($records) 
        {
            foreach ($records as $rec) 
            {
                $logs[] = array(
                    'uid' => $rec->user->id,
                    'first_name' => $rec->user->first_name,
                    'last_name' => $rec->user->last_name,
                    'middle_name' => $rec->user->middle_name,
                    'time_in' => $rec->created_at->format('h:i:s A'),
                    'created_at' => $rec->created_at,
                    'image' => $rec->user->profile_image,
                    'logout' => $rec->logout,
                    'login' => $rec->login,
                );

                if ( $rec->logout ) {
                    $uids_out[] = $rec->user->id;
                }

                if ( $rec->login ) {
                    $uids_in[] = $rec->user->id;
                }
            }
        }

        $department = Department::find($id);

        return view('scanner',compact('users','logs','uids_in','uids_out','department'));
    }

    public function log(Request $request)
    {
        $return = array(
            'error' => true,
            'loggedin' => false,
            'message' => 'Please try again.'
        );

        $id = $request->input('id');
        $time = $request->input('time');

        $types = ['in','out'];

        if ( $id && in_array($time, $types) ) 
        {
            $user = User::find($id);

            if (!$user) { return $return; }

            $where = [];
            $where[] = ['user_id', '=', $user->id];

            $hasLog = Log::where('user_id', '=', $user->id)
                ->whereDate('created_at', Carbon::today())
                ->latest('created_at')
                ->first();

            if ( $time == 'in' ) {
                
                // $where[] = ['login', '=', true]; 
                if ( $hasLog ) {
                    if ( $hasLog->login ) {

                        $return['message'] = 'Log already exist.';
                        return $return;

                    } else {

                        $return['message'] = (strtotime("-2 minutes") < strtotime($hasLog->created_at));

                        if ( strtotime("-2 minutes") < strtotime($hasLog->created_at) ) {
                            $return['message'] = 'Please wait two minutes.';
                            return $return;
                        }
                    }
                }

                $isAbsent = Absent::where('user_id','=',$user->id)->whereDate('created_at', Carbon::today())->first();
                // create record if record does not exist
                if ( !$isAbsent ) 
                {
                    $absent = new Absent;
                    $absent->absent = $user->isAbsent();
                    $absent->user_id = $user->id;
                    $absent->save();

                } else {

                    $isAbsent->absent = false;
                    $isAbsent->save();
                }
            } 

            if ( $time == 'out' ) {
                
                if ( $hasLog ) {

                    if ( $hasLog->login  ) {

                        if ( strtotime("-2 minutes") < strtotime($hasLog->created_at) ) {
                            $return['message'] = 'Please wait two minute.';
                            return $return;
                        }

                    } else {

                        $return['message'] = 'You already logged out.';
                        return $return;
                    }
                }

                if ( $user->isAbsent() ) {
                    $return['message'] = $user->first_name.' has not timed in yet for the day.';
                    return $return;
                }

                //$where[] = ['logout', '=', true]; 
            }

            $log = new Log();

            switch ($time) 
            {
                case 'in':
                    $log->login = true;
                    $return['loggedin'] = true;
                    break;
                case 'out':
                    $log->logout = true;
                    break;
            }

            $log->user_id = $user->id;
            $log->save();

            $return['error'] = false;
            $return['message'] = 'Log successfully recorded';

            $records = Log::whereDate('created_at', Carbon::today())->orderBy('created_at','desc')->get();

            $logs = [];
            $uids_out = [];
            $uids_in = [];

            if ($records) {
                foreach ($records as $rec) {
                    $logs[] = array(
                        'uid' => $rec->user->id,
                        'first_name' => $rec->user->first_name,
                        'last_name' => $rec->user->last_name,
                        'middle_name' => $rec->user->middle_name,
                        'time_in' => $rec->created_at->format('h:i:s A'),
                        'created_at' => $rec->created_at,
                        'image' => $rec->user->profile_image,
                        'logout' => $rec->logout,
                        'login' => $rec->login,
                    );

                    if ( $rec->logout ) {
                        $uids_out[] = $rec->user->id;
                    }

                    if ( $rec->login ) {
                        $uids_in[] = $rec->user->id;
                    }
                }
            }

            $return['records'] = $logs;
            $return['uids_in'] = $uids_in;
            $return['uids_out'] = $uids_out;
        }
    
        return $return;
    }

    public function timein(){

        $users = User::orderBy('last_name','DESC')->whereHas('roles', function($q){
            $q->where('name', 'Faculty');
        })->get(['id','profile_image','first_name','last_name','middle_name','face_up','face_down','face_left','face_right','lean_left','lean_right'])->toArray();

        $logs = [];
        $uids = [];

        $records = Log::where('login', '=', true)->whereDate('created_at', Carbon::today())->get();

        if ($records) {
            foreach ($records as $rec) {
                $logs[] = array(
                    'first_name' => $rec->user->first_name,
                    'last_name' => $rec->user->last_name,
                    'middle_name' => $rec->user->middle_name,
                    'time_in' => $rec->created_at->format('h:i:s A'),
                );

                $uids[] = $rec->user->id;
            }
        }

        return view('time-in',compact('users','logs','uids'));
    }

    public function timeout(){

        $users = User::orderBy('last_name','DESC')->whereHas('roles', function($q){
            $q->where('name', 'Faculty');
        })->get(['id','profile_image','first_name','last_name','middle_name','face_up','face_down','face_left','face_right','lean_left','lean_right'])->toArray();

        $logs = [];
        $uids = [];

        $records = Log::where('logout', '=', true)->whereDate('created_at', Carbon::today())->get();

        if ($records) {
            foreach ($records as $rec) {
                $logs[] = array(
                    'first_name' => $rec->user->first_name,
                    'last_name' => $rec->user->last_name,
                    'middle_name' => $rec->user->middle_name,
                    'time_in' => $rec->created_at->format('h:i:s A'),
                );

                $uids[] = $rec->user->id;
            }
        }

        return view('time-out',compact('users','logs','uids'));
    }

    public function autofill(Request $request)
    {   
        $month = ($request->input('month')) ? $request->input('month') : date('m');
        $year = ($request->input('year')) ? $request->input('year') : date('Y');

        $numdays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $days = [];

        // $user = User::find($uid);
        $users = User::orderBy('last_name','DESC')->whereHas('roles', function($q){
            $q->where('name', 'Faculty');
        })->get();

        if ($users) {

            $dumps = '';

            foreach ($users as $user) 
            {
                for( $d=1; $d <= $numdays; $d++ )
                {
                    $time = mktime(12, 0, 0, $month, $d, $year);
                    if (date('m', $time)==$month) 
                    {   
                        if ( !in_array(date('D', $time), ['Sat','Sun']) )  {

                            // $days[date('m/d/Y D', $time)]
                            $amlogin = new Log();
                            $amlogin->created_at = date('Y-m-d', $time).' 0'.rand(6, 9).':'.rand(10, 59).':'.rand(10, 59);
                            $amlogin->login = true;
                            $amlogin->user_id = $user->id;
                            $amlogin->save();

                            $amlogout = new Log();
                            $amlogout->created_at = date('Y-m-d', $time).' '.rand(10, 11).':'.rand(10, 59).':'.rand(10, 59);
                            $amlogout->logout = true;
                            $amlogout->user_id = $user->id;
                            $amlogout->save();

                            $pmlogin = new Log();
                            $pmlogin->created_at = date('Y-m-d', $time).' '.rand(12, 15).':'.rand(10, 59).':'.rand(10, 59);
                            $pmlogin->login = true;
                            $pmlogin->user_id = $user->id;
                            $pmlogin->save();

                            $pmlogout = new Log();
                            $pmlogout->created_at = date('Y-m-d', $time).' '.rand(14, 17).':'.rand(10, 59).':'.rand(10, 59);
                            $pmlogout->logout = true;
                            $pmlogout->user_id = $user->id;
                            $pmlogout->save();

                        }
                    }
                }

                $dumps .= $user->first_name.'<br>';
            }

            echo $dumps;
            
        } else {

            echo 'Users not found.';
        }
    }

    public function clear(){

        Log::truncate();

        return 'All logs are cleared.<br><br><a href="/">Go Back</a>';
    }
}
