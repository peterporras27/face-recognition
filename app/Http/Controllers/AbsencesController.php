<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Absent;
use App\Models\User;

class AbsencesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:absent-list|absent-create|absent-edit|absent-delete', ['only' => ['index','show']]);
         $this->middleware('permission:absent-create', ['only' => ['create','store']]);
         $this->middleware('permission:absent-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:absent-delete', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $absences = Absent::latest()->paginate(10);
        return view('absences.index',compact('absences'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = User::where('role','=','employee')->get();
        return view('absences.create', compact('employees'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'date' => 'required',
            'cause' => 'required',
            'reason' => 'required',
            'user_id' => 'required',
        ]);
    
        Absent::create($request->all());
    
        return redirect()->route('absences.index')
                        ->with('success','Absent created successfully.');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Absent  $absent
     * @return \Illuminate\Http\Response
     */
    public function show(Absent $absent)
    {
        return view('absences.show',compact('absent'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Absent  $absent
     * @return \Illuminate\Http\Response
     */
    public function edit(Absent $absent)
    {
        $employees = User::where('role','=','employee')->get();
        return view('absences.edit',compact('absent','employees'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Absent  $absent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Absent $absent)
    {
         request()->validate([
            'date' => 'required',
            'cause' => 'required',
            'reason' => 'required',
            'user_id' => 'required',
        ]);
    
        $absent->update($request->all());
    
        return redirect()->route('absences.index')
                        ->with('success','Absent updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Absent  $absent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Absent $absent)
    {
        $absent->delete();
    
        return redirect()->route('absences.index')
                        ->with('success','Absent deleted successfully');
    }
}
