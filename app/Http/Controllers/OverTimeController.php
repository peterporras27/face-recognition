<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Overtime;
use App\Models\User;

class OverTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:overtime-list|overtime-create|overtime-edit|overtime-delete', ['only' => ['index','show']]);
         $this->middleware('permission:overtime-create', ['only' => ['create','store']]);
         $this->middleware('permission:overtime-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:overtime-delete', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $overtime = OverTime::latest()->paginate(10);
        return view('overtime.index',compact('overtime'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('overtime.index');
        // return view('overtime.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'time' => 'required',
            'user_id' => 'required|integer',
        ]);
    
        OverTime::create($request->all());
    
        return redirect()->route('overtime.index')
                        ->with('success','OverTime created successfully.');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\OverTime  $overtime
     * @return \Illuminate\Http\Response
     */
    public function show(OverTime $overtime)
    {
        return redirect()->route('overtime.index');
        // return view('overtime.show',compact('overtime'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OverTime  $overtime
     * @return \Illuminate\Http\Response
     */
    public function edit(OverTime $overtime)
    {
        $employees = User::where('role','=','employee')->get();
        return view('overtime.edit',compact('overtime','employees'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OverTime  $overtime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OverTime $overtime)
    {
         request()->validate([
            'time' => 'required',
            'user_id' => 'required|integer',
        ]);
    
        $overtime->update($request->all());
    
        return redirect()->route('overtime.index')
                        ->with('success','OverTime updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OverTime  $overtime
     * @return \Illuminate\Http\Response
     */
    public function destroy(OverTime $overtime)
    {
        $overtime->delete();
    
        return redirect()->route('overtime.index')
                        ->with('success','OverTime deleted successfully');
    }
}
