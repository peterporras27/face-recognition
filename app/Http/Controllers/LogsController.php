<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Log;

class LogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:log-list|log-create|log-edit|log-delete', ['only' => ['index','show']]);
         $this->middleware('permission:log-create', ['only' => ['create','store']]);
         $this->middleware('permission:log-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:log-delete', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $uid = $request->input('uid');
        $numdays = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));

        $from = date('Y-m-').'01';
        $to = date('Y-m-').$numdays;

        $perpage = $request->input('perpage') ?  preg_replace('/\D/', '', $request->input('perpage')):20;
        
        $uid = $request->input('uid');

        if ( $request->input('from') && $request->input('to') ) 
        {
            $from = $request->input('from'); 
            $to = $request->input('to');
        }

        $users = User::orderBy('last_name','DESC')->whereHas('roles', function($q){
            $q->where('name', 'Faculty');
        })->get();        

        $logs = Log::whereBetween('created_at', [$from, $to])->get();
        $view = 'logs.index';
        if ($request->input('print')) {
            $view = 'logs.print';
        }

        return view($view,compact('users','logs','from','to','uid','perpage'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('logs.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'login' => 'required',
            'logout' => 'required',
            'user_id' => 'required'
        ]);
    
        Log::create($request->all());
    
        return redirect()->route('logs.index')
                        ->with('success','Log created successfully.');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Log  $log
     * @return \Illuminate\Http\Response
     */
    public function show(Log $log)
    {
        return view('logs.show',compact('log'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Log  $log
     * @return \Illuminate\Http\Response
     */
    public function edit(Log $log)
    {
        return view('logs.edit',compact('log'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Log  $log
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Log $log)
    {
         request()->validate([
            'login' => 'required',
            'logout' => 'required',
            'user_id' => 'required'
        ]);
    
        $log->update($request->all());
    
        return redirect()->route('logs.index')
                        ->with('success','Log updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Log  $log
     * @return \Illuminate\Http\Response
     */
    public function destroy(Log $log)
    {
        $log->delete();
    
        return redirect()->route('logs.index')
                        ->with('success','Log deleted successfully');
    }
}
