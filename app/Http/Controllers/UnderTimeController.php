<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UnderTime;
use App\Models\User;

class UnderTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:undertime-list|undertime-create|undertime-edit|undertime-delete', ['only' => ['index','show']]);
         $this->middleware('permission:undertime-create', ['only' => ['create','store']]);
         $this->middleware('permission:undertime-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:undertime-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $undertime = UnderTime::latest()->paginate(10);
        return view('undertime.index',compact('undertime'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('undertime.index');
        // return view('undertime.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'time' => 'required',
            'user_id' => 'required|integer',
        ]);
    
        UnderTime::create($request->all());
    
        return redirect()->route('undertime.index')
                        ->with('success','UnderTime created successfully.');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\UnderTime  $undertime
     * @return \Illuminate\Http\Response
     */
    public function show(UnderTime $undertime)
    {
        return redirect()->route('undertime.index');
        // return view('undertime.show',compact('undertime'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UnderTime  $undertime
     * @return \Illuminate\Http\Response
     */
    public function edit(UnderTime $undertime)
    {
        $employees = User::where('role','=','employee')->get();
        return view('undertime.edit',compact('undertime','employees'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UnderTime  $undertime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UnderTime $undertime)
    {
         request()->validate([
            'time' => 'required',
            'user_id' => 'required|integer',
        ]);
    
        $undertime->update($request->all());
    
        return redirect()->route('undertime.index')
                        ->with('success','UnderTime updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UnderTime  $undertime
     * @return \Illuminate\Http\Response
     */
    public function destroy(UnderTime $undertime)
    {
        $undertime->delete();
    
        return redirect()->route('undertime.index')
                        ->with('success','UnderTime deleted successfully');
    }
}
