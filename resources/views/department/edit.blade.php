@extends('layouts.neon')

@section('title')
<div class="pull-left">
    <h2>Edit User</h2>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <a class="btn btn-default" href="{{ route('departments.index') }}"><i class="entypo-left-bold"></i> Go Back</a>
    </div>
</div>

<hr>
@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

{!! Form::model($department, ['method' => 'PATCH','enctype'=>'multipart/form-data','route' => ['departments.update', $department->id]]) !!}


<ul class="nav nav-tabs bordered">
    <li class="active">
        <a href="#details" data-toggle="tab">
            <span class="visible-xs"><i class="entypo-home"></i></span>
            <span class="hidden-xs">User Details</span>
        </a>
    </li>
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="details">
        <br>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <strong>First Name:</strong>
                    {!! Form::text('name', null, array('placeholder' => 'School of ...','class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    <strong>Last Name:</strong>
                    {!! Form::text('abrevation', null, array('placeholder' => 'SO..','class' => 'form-control')) !!}
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            </div>
        </div>
    </div>
    
</div>

<button type="submit" class="btn btn-primary btn-lg btn-icon">Save <i class="entypo-floppy"></i></button>

{!! Form::close() !!}


@endsection