@extends('layouts.neon')

@section('title')
<div class="pull-left">
    <h2>Departments</h2>
</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <a class="btn btn-success" href="{{ route('departments.create') }}"> Register New Department</a>
        </div>
    </div>
    <hr>

    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif


    <table class="table table-bordered">
        <tr>
            <th>Name</th>
            <th>Abrevation</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($data as $key => $department)
        <tr>
            <td>{{ $department->name }}</td>
            <td>{{ $department->abrevation }}</td>
            <td>
                <a class="btn btn-primary btn-xs" href="{{ route('departments.edit',$department->id) }}">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['departments.destroy', $department->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </table>


    {!! $data->render() !!}

@endsection