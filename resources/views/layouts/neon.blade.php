<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="SG Reporting" />
	<meta name="author" content="" />

	<link rel="icon" href="{{asset('assets/images/favicon.ico')}}">

	<title>{{ config('app.name', 'SG Reporting') }}</title>

	<link rel="stylesheet" href="{{asset('assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/font-icons/entypo/css/entypo.css')}}">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/neon-core.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/neon-theme.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/neon-forms.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
	<link rel="stylesheet" href="{{asset('css/bootstrap-tagsinput.css')}}">

	<script src="{{asset('assets/js/jquery-1.11.3.min.js')}}"></script>

	<!--[if lt IE 9]><script src="{{asset('assets/js/ie8-responsive-file-warning.js')}}"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	@yield('header')
	{{-- <style>
	footer.main{
		position: absolute;
    	bottom: 10px;
    	width: 97%;
    }
    </style> --}}

</head>
<body class="page-body  page-left-in" data-url="https://reporting.seogstage.com">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<div class="sidebar-menu">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a href="/">
						<img src="{{ asset('assets/images/logo.png') }}" alt="">
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>
								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>

			</header>
			
									
			<ul id="main-menu" class="main-menu">
				<!-- add class "multiple-expanded" to allow multiple submenus to open -->
				<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

				<li class="{{ request()->is('dashboard*') ? 'active' : '' }}">
					<a href="{{ route('dashboard') }}">
						<i class="entypo-gauge"></i>
						<span class="title">Dashboard</span>
					</a>
				</li>

				<li class="{{ request()->is('departments*') ? 'active' : '' }}">
					<a href="{{ route('departments.index') }}">
						<i class="entypo-home"></i>
						<span class="title">Departments</span>
					</a>
				</li>

				@can('user-list')
				<li class="{{ request()->is('users*') ? 'active' : '' }}">
					<a href="{{ route('users.index') }}">
						<i class="entypo-users"></i>
						<span class="title">Users / Employees</span>
					</a>
				</li>
				@endcan

				@can('log-list')
				<li class="{{ request()->is('logs*') ? 'active' : '' }}">
					<a href="{{ route('logs.index') }}">
						<i class="entypo-archive"></i>
						<span class="title">Logs</span>
					</a>
				</li>
				@endcan

				{{-- <li class="{{ request()->is('absences*') ? 'active' : '' }}">
					<a href="{{ route('absences.index') }}">
						<i class="entypo-attention"></i>
						<span class="title">Absences</span>
					</a>
				</li>

				<li class="{{ request()->is('undertime*') ? 'active' : '' }}">
					<a href="{{ route('undertime.index') }}">
						<i class="entypo-download"></i>
						<span class="title">Undertime</span>
					</a>
				</li>

				<li class="{{ request()->is('overtime*') ? 'active' : '' }}">
					<a href="{{ route('overtime.index') }}">
						<i class="entypo-upload"></i>
						<span class="title">Overtime</span>
					</a>
				</li> --}}

				@can('role-list')
				<li class="{{ request()->is('roles*') ? 'active' : '' }}">
					<a href="{{ route('roles.index') }}">
						<i class="entypo-lock-open"></i>
						<span class="title">User Roles</span>
					</a>
				</li>
				@endcan

			</ul>
			
		</div>

	</div>

	<div class="main-content">
				
		<div class="row">
		
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
				@yield('title')
			</div>
		
			<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix hidden-xs">
		
				<ul class="list-inline links-list pull-right">
		
					<li class="sep"></li>
		
					<li>
						<a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
							{{ __('Log Out') }} <i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
			</div>
		
		</div>
		
		<hr />
	
		@yield('content')

		<!-- Footer -->
		<footer class="main">
			
			&copy; {{ date('Y') }} 
		
		</footer>
	</div>
</div>

	<script src="{{asset('assets/js/gsap/TweenMax.min.js')}}"></script>
	<script src="{{asset('assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
	<script src="{{asset('assets/js/bootstrap.js')}}"></script>
	<script src="{{asset('assets/js/joinable.js')}}"></script>
	<script src="{{asset('assets/js/resizeable.js')}}"></script>
	<script src="{{asset('assets/js/neon-api.js')}}"></script>

	<script src="{{asset('assets/js/neon-custom.js')}}"></script>
	<script src="{{asset('assets/js/neon-demo.js')}}"></script>
	<script src="{{asset('js/bootstrap-tagsinput.min.js')}}"></script>
	@yield('footer')
</body>
</html>