@extends('layouts.neon')

@section('title')
<h2>{{ date('F j, Y (l)') }}</h2>
@endsection

@section('content')

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif


    <div class="row">

        @if($users->count())

            <?php 
            $numOfCols = 4;
            $rowCount = 0;
            $bootstrapColWidth = 12 / $numOfCols;
            ?>

            @foreach($users as $user)

                <div class="col-sm-3">

                    <div class="tile-stats tile-white-cyan">
                        <div class="icon"><i class="entypo-archive"></i></div>
                        @if($user->profile_image)
                        <div class="row">
                            <div class="col-md-3">
                                <div class="user-thumb"> 
                                    <a href="#">
                                        <img src="{{ asset('uploads/images/'.$user->profile_image) }}" width="44" alt="" class="img-circle">
                                    </a> 
                                </div>
                            </div>
                            <div class="col-md-9">
                                <a href="{{ route('users.show',$user->id) }}">
                                    <h3>{{ $user->first_name.' '.$user->middle_name.' '.$user->last_name }} </h3>
                                </a>
                                
                                @if($user->isAbsent())
                                <p><span class="badge badge-danger">Absent</span></p>
                                @else
                                <p><span><b>Login:</b> {{ $user->timeIn()->created_at->format('h:i:s A') }}</span></p>
                                @endif

                                @if($user->timeOut())
                                <p><b>Logout:</b> {{ $user->timeOut()->created_at->format('h:i:s A') }}</p>
                                @endif
                            </div>
                        </div>
                        @else
                        {{-- <div class="num">83</div> --}}
                        <a href="{{ route('users.show',$user->id) }}">
                            <h3>{{ $user->first_name.' '.$user->middle_name.' '.$user->last_name }}</h3>
                            <p>{{ $user->department }}</p>
                        </a>
                        @endif
                        
                    </div>
                    
                </div>

                <?php $rowCount++; ?> 

                @if($rowCount % $numOfCols == 0) 
                    </div><div class="row">
                @endif
            
            @endforeach

            {!! $users->render() !!}
        @endif
    </div>
    

@endsection
