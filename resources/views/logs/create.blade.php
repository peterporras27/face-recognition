@extends('layouts.neon')

@section('title')
<div class="pull-left">
    <h2>Add New Client</h2>
</div>
@endsection


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            
            <a class="btn btn-default" href="{{ route('clients.index') }}"><i class="entypo-left-bold"></i> Go Back</a>
            
        </div>
    </div>

    <hr>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('clients.store') }}" method="POST">
    	@csrf


         <div class="row">
		    <div class="col-xs-12 col-sm-6 col-md-6">
		        <div class="form-group">
		            <strong>Name:</strong>
		            <input type="text" name="name" class="form-control" placeholder="Name">
		        </div>
		    </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Email:</strong>
                    <input type="email" name="email" class="form-control" placeholder="">
                </div>
            </div>
		    <div class="col-xs-12 col-sm-12 col-md-12">
                <button type="submit" class="btn btn-primary">Submit</button>
		    </div>
		</div>


    </form>

@endsection