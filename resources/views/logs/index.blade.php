@extends('layouts.neon')

@section('title')
<div class="pull-left">
    <h2>Employees Record Logs</h2>
</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="float-right">
                @can('log-create')
                {{-- <a class="btn btn-success" href="{{ route('logs.create') }}"> Create Log</a> --}}
                @endcan
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="card">
        <div class="card-body">

            <form method="GET">
                <div class="row">

                    <div class="col-md-3">
                        <div class="input-group"> 
                            <span class="input-group-addon">From:</span> 
                            <input type="date" name="from" value="{{ $from }}" class="form-control"> 
                        </div> 
                        <br>
                        <div class="input-group"> 
                            <span class="input-group-addon">Results per page:</span> 
                            <input type="number" min="10" name="perpage" value="{{ $perpage }}" class="form-control"> 
                        </div> 
                    </div>
                    <div class="col-md-3">
                        <div class="input-group"> 
                            <span class="input-group-addon">To:</span> 
                            <input type="date" name="to" value="{{ $to }}" class="form-control"> 
                        </div> 
                        <br>
                        <div class="input-group"> 
                            <span class="input-group-addon">User:</span> 
                            <select name="uid" class="form-control">
                                <option value="">All</option>
                                @if($users)
                                    @foreach( $users as $user )
                                        <option value="{{ $user->id }}"{{ $uid == $user->id ? ' selected':'' }}>{{ $user->first_name.' '.$user->last_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div> 
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-primary btn-icon">Filter <i class="entypo-search"></i></button>
                        <br><br>
                        <a href="{{ route('logs.index') }}" class="btn btn-default btn-icon">Reset Filter <i class="entypo-arrows-ccw"></i></a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ route('logs.index').'?'.http_build_query(request()->query()) }}&print=1" target="_blank" class="btn btn-info pull-right btn-icon">Print <i class="entypo-print"></i></a>
                    </div>

                </div>
            </form>

            <hr>

            @if( $users->count() > 0 )
            
                <table id="forprint" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Names</th>
                            <th>ID Number</th>
                            <th>Absences No. of days</th>
                            <th>Absences dates</th>
                            <th>Undertime No. of times</th>
                            <th>Undertime Total No. hrs/mins.</th>
                            <th>Undertime Dates</th>
                            <th>Vacation Leave</th>
                            <th>Sick leave</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $users as $usr )
                        <?php $record = $usr->noAbsences($from,$to); ?>
                        <tr>
                            <td>{{ $usr->first_name.' '.$usr->last_name }}</td>
                            <td>{{ $usr->department }}</td>
                            <td>{{ $record['absences'] }}</td>
                            <td>{{ implode(',', $record['ab_dates']) }}</td>
                            <td>{{ $record['ut_count'] }}</td>
                            <td>{{ $record['utime']['hours'].':'.$record['utime']['minutes'].':'.$record['utime']['seconds'] }}</td>
                            <td>{{ implode(',', $record['ut_dates']) }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            @else

                <div class="alert alert-info alert-dismissible show" role="alert">
                  There are no available data to show at the moment.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>

            @endif

        </div>
    </div>

@endsection