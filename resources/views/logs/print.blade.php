<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Attendance Log</title>
		<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	</head>
	<body>
		<div class="container">
			<h3>Attendance Log</h3>
			<p>
			@if($from) <b>From:</b> {{ date('F, j Y', strtotime($from)) }} @endif
			@if($to) <b>To:</b> {{ date('F, j Y', strtotime($to)) }} @endif
			</p>

			@if( $logs->count() )
                <?php 
                
                $datas = [];
                foreach( $logs as $log ) 
                {
                    $date = $log->created_at->format('Y-m-d');
                    $datas[$log->user_id][$date]['first_name'] = $log->user->first_name;
                    $datas[$log->user_id][$date]['last_name'] = $log->user->last_name;
                    $datas[$log->user_id][$date]['middle_name'] = $log->user->middle_name;
                    $datas[$log->user_id][$date]['date'] = $log->created_at->format('F, j Y');

                    if ( $log->login ) {
                        $datas[$log->user_id][$date]['login'] = $log->created_at->format('h:i:s A');
                    }

                    if ( $log->logout ) {
                        $datas[$log->user_id][$date]['logout'] = $log->created_at->format('h:i:s A');
                    }

                    $datas[$log->user_id][$date]['times'] = $log->user->timeDiff($log->created_at);

                } ?>
            
                <table id="forprint" class="table table-bordered">
                    <tr>
                        <th>Employee</th>
                        <th>Date</th>
                        <th>Login</th>
                        <th>Logout</th>
                        <th>Undertime</th>
                        <th>Overtime</th>
                    </tr>
                    @foreach ($datas as $data => $l)
                        @foreach($l as $t => $v)
                            <tr>
                                <td>{{ $v['first_name'].' '.$v['middle_name'].' '.$v['last_name'] }}</td>
                                <td>{{ $v['date'] }}</td>
                                <td>{{ (isset($v['login'])) ? $v['login']:'' }}</td>
                                <td>{{ (isset($v['logout'])) ? $v['logout']:'' }}</td>
                                <td>{{ $v['times']['undertime']['hours'].':'.$v['times']['undertime']['minutes'] }}</td>
                                <td>{{ $v['times']['overtime']['hours'].':'.$v['times']['overtime']['minutes'] }}</td>
                            </tr>
                        @endforeach
                    @endforeach
                </table>

            @endif
        </div>
        <script>window.print();</script>
	</body>
</html>