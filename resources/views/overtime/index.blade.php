@extends('layouts.neon')

@section('title')
<div class="pull-left">
    <h2>Overtime Records</h2>
</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="float-right">
                @can('overtime-create')
                {{-- <a class="btn btn-success" href="{{ route('overtime.create') }}"> Create New Record</a> --}}
                @endcan
            </div>
        </div>
    </div>

    {{-- <hr> --}}
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="card">
        <div class="card-body">

            @if( $overtime->count() )

                <table class="table table-bordered">
                    <tr>
                        <th>Employee</th>
                        <th>Time</th>
                        <th width="280px">Action</th>
                    </tr>
                    @foreach ($overtime as $record)
                    <tr>
                        <td>{{ $record->user->last_name.', '.$record->user->first_name.' '.$record->user->middle_name }}</td>
                        <td>{{ $record->time }}</td>
                        <td>
                            <form action="{{ route('overtime.destroy',$record->id) }}" method="POST">
                                <a class="btn btn-info btn-xs" href="{{ route('overtime.show',$record->id) }}">Details</a>
                                @can('overtime-edit')
                                <a class="btn btn-primary btn-xs" href="{{ route('overtime.edit',$record->id) }}">Edit</a>
                                @endcan
                                @csrf
                                @method('DELETE')
                                @can('overtime-delete')
                                <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                                @endcan
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>

                {!! $overtime->links() !!}

            @else

                <div class="alert alert-info alert-dismissible show" role="alert">
                  There are no available data to show at the moment.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>

            @endif

        </div>
    </div>

    

@endsection