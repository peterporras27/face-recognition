@extends('layouts.neon')


@section('title')
<div class="pull-left">
    <h2>Edit Overtime</h2>
</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            
            <a class="btn btn-default" href="{{ route('overtime.index') }}"><i class="entypo-left-bold"></i> Go Back</a>
            
        </div>
    </div>
    <hr>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('overtime.update',$overtime->id) }}" method="POST">
    	@csrf
        @method('PUT')


        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Time:</strong>
                    <input type="text" name="name" value="{{ $overtime->time }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Employee:</strong>
                    <select name="user_id" class="form-control">
                        @foreach($employees as $employee)
                        <option value="{{ $employee->id }}" {{ $overtime->user_id == $employee->id ? ' selected':'' }}>{{ $employee->last_name.', '.$employee->first_name.' '.$employee->middle_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

		    <div class="col-xs-12 col-sm-12 col-md-12">
		      <button type="submit" class="btn btn-primary">Save</button>
		    </div>
		</div>


    </form>

@endsection