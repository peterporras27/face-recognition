@extends('layouts.neon')

@section('title')
<div class="pull-left">
    <h2>Records of Absences</h2>
</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="float-right">
                @can('absent-create')
                <a class="btn btn-success" href="{{ route('absences.create') }}"> Create new record</a>
                @endcan
            </div>
        </div>
    </div>

    <hr>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="card">
        <div class="card-body">

            @if( $absences->count() )

                <table class="table table-bordered">
                    <tr>
                        <th>Employee</th>
                        <th>Date</th>
                        <th>Cause</th>
                        <th width="280px">Action</th>
                    </tr>
                    @foreach ($absences as $absent)
                    <tr>
                        <td>{{ $absent->user->last_name.', '.$absent->user->first_name.' '.$absent->user->middle_name }}</td>
                        <td>{{ $absent->date }}</td>
                        <td>{{ ucfirst( str_replace('-', ' ', $absent->cause) ) }}</td>
                        <td>
                            <form action="{{ route('absences.destroy',$absent->id) }}" method="POST">
                                {{-- <a class="btn btn-info btn-xs" href="{{ route('absences.show',$absent->id) }}">Details</a> --}}
                                @can('absent-edit')
                                <a class="btn btn-primary btn-xs" href="{{ route('absences.edit',$absent->id) }}">Edit</a>
                                @endcan
                                @csrf
                                @method('DELETE')
                                @can('absent-delete')
                                <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                                @endcan
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>

                {!! $absences->links() !!}

            @else

                <div class="alert alert-info alert-dismissible show" role="alert">
                  There are no available data to show at the moment.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>

            @endif

        </div>
    </div>

    

@endsection