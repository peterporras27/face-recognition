@extends('layouts.neon')

@section('title')
<div class="pull-left">
    <h2>Create a Record of Absence</h2>
</div>
@endsection


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            
            <a class="btn btn-default" href="{{ route('absences.index') }}"><i class="entypo-left-bold"></i> Go Back</a>
            
        </div>
    </div>

    <hr>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('absences.store') }}" method="POST">
    	@csrf
        <div class="row">
		    <div class="col-xs-12 col-sm-6 col-md-6">
		        <div class="form-group">
		            <strong>Date:</strong>
		            <input type="text" name="date" class="form-control" placeholder="{{ date('m/d/Y') }}">
		        </div>
		    </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Cause:</strong>
                    <select name="cause" class="form-control">
                        <option value="other">Other</option>
                        <option value="sick-leave">Sick Leave</option>
                        <option value="vacation-leave">Vacation Leave</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Reason:</strong>
                    <textarea name="reason" rows="3" class="form-control" placeholder="Other reasons for being absent."></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Employee:</strong>
                    <select name="user_id" class="form-control">
                        @foreach($employees as $employee)
                        <option value="{{ $employee->id }}">{{ $employee->last_name.', '.$employee->first_name.' '.$employee->middle_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
		    <div class="col-xs-12 col-sm-12 col-md-12">
                <button type="submit" class="btn btn-primary">Submit</button>
		    </div>
		</div>
    </form>

@endsection