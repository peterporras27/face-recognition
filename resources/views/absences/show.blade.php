@extends('layouts.neon')

@section('title')
<div class="pull-left">
    <h2> Show Client</h2>
</div>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <a class="btn btn-default" href="{{ route('clients.index') }}"><i class="entypo-left-bold"></i> Go Back</a>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $client->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                {{ $client->email }}
            </div>
        </div>
        
    </div>

@endsection