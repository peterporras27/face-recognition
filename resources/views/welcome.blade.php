<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>WVSU Face Login Recognition</title>
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <style>
            .navbar-brand{color:#fff;}
            .nav-item a{color:#fff;}
            #overlay{top: 0;position: absolute;left: 0;}
            video,canvas{width: 100%;}
        </style>
    </head>
    <body>
        
        <nav class="navbar navbar-expand-lg navbar-dard bg-dark">
            <a class="navbar-brand" href="/">Face Login Recognition</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto my-2 my-lg-0">
                    <li class="nav-item">
                        @if( auth()->check() )
                        <a href="{{ route('dashboard') }}" class="nav-link"><small>Admin Panel</small></a>
                        @else
                        <a href="{{ route('login') }}" class="nav-link"><small>Admin Login</small></a>
                        @endif
                    </li>
                </ul>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col mt-5 text-center">
                    <div class="row">
                        <?php
                        //Columns must be a factor of 12 (1,2,3,4,6,12)
                        $numOfCols = 3;
                        $rowCount = 0;
                        $bootstrapColWidth = 12 / $numOfCols;
                        ?>
                        @foreach($departments as $dep)
                            <div class="col-md-<?php echo $bootstrapColWidth; ?> mb-5">
                                <div class="card bg-default border-default" style="max-width: 18rem;">
                                    <div class="card-header"><b>{{ $dep->abrevation }}</b></div>
                                    <div class="card-body text-default">
                                        {{ $dep->name }}
                                    </div>
                                    <div class="card-footer">
                                        <a href="{{ route('scanner',$dep->id) }}" class="btn btn-block btn-info">TIME IN / OUT</a>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $rowCount++;
                            if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
                            ?>
                        @endforeach
                    </div>
                </div>
            </div>
            
        </div>
        
        <script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        
    </body>
</html>