<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>WVSU Face Login Recognition</title>
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <style>
            .navbar-brand{color:#fff;}
            .nav-item a{color:#fff;}
            #overlay{top: 0;position: absolute;left: 0;}
            video,canvas{width: 100%;}
            .loader {
                border: 16px solid #f3f3f3;
                border-radius: 50%;
                border-top: 16px solid #3498db;
                width: 120px;
                height: 120px;
                -webkit-animation: spin 2s linear infinite; /* Safari */
                animation: spin 2s linear infinite;
            }

            .loading{
                text-align: center;
                position: absolute;
                top: 40%;
                left: 40%;

            }

            @-webkit-keyframes spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }
        </style>
    </head>
    <body>
        
        <nav class="navbar navbar-expand-lg navbar-dard bg-dark">
            <a class="navbar-brand" href="/">Face Login Recognition</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto my-2 my-lg-0">
                    <li class="nav-item">
                        @if( auth()->check() )
                        <a href="{{ route('dashboard') }}" class="nav-link"><small>Admin Panel</small></a>
                        @else
                        <a href="{{ route('login') }}" class="nav-link"><small>Admin Login</small></a>
                        @endif
                    </li>
                </ul>
            </div>
        </nav>

        <div class="container-fluid">
            <h3 class="text-center mt-3">{{ $department->name }}</h3>
            <hr>
            <div class="row">
                <div class="col-md-6 mt-2">

                    <div class="card">
                        <div class="card-header">
                            Camera <span class="float-right"><small>(Time: <span id="time"></span> FPS: <span id="fps"></span>)</small></span>
                        </div> 
                        <div class="card-body p-0">
                            <div id="alert"></div>
                            <div style="position: relative" class="margin">
                                <video onloadedmetadata="onPlay(this)" id="inputVideo" autoplay muted playsinline></video>
                                <canvas id="overlay" />
                            </div>
                            <div class="loading">
                                <div class="loader"></div>
                                <p>Please Wait...</p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 mt-2">
                    <div class="card">
                        <div class="card-header">
                            <b>Time In</b> <span class="float-right">{{ date('F j, Y (l)') }} <span class="time"></span></span>
                        </div>
                        <div class="card-body">
                            <div id="loggedin">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('face-api/js/face-api.min.js')}}"></script>
        <script src="{{ asset('face-api/js/commons.js')}}"></script>
        <script src="{{ asset('face-api/js/faceDetectionControls.js')}}"></script>
        <script src="{{ asset('face-api/js/imageSelectionControls.js')}}"></script>
        <script src="{{ asset('face-api/js/bbt.js')}}"></script>
        <script>
            var storedLabel = 'userlogs';
            var faculty = {!! json_encode($users); !!};
            var theday = '{{ date('Y-m-d') }}';
            var labeledDescriptors = [];
            var faceMatcher = null;

            var test = [];
            var logins = [];
            var logout = [];

            var runAjax = [];
            let forwardTimes = [];
            var listener = setInterval(logUser, 2000);
            var full_name = '';

            localStorage.setItem(storedLabel, JSON.stringify({!! json_encode($logs) !!}));
            // localStorage.setItem('uids_in', JSON.stringify({!! json_encode($uids_in) !!}));
            // localStorage.setItem('uids_out', JSON.stringify({!! json_encode($uids_out) !!}));

            var stored = JSON.parse(localStorage.getItem(storedLabel));
            // var uids_login = JSON.parse(localStorage.getItem('uids_in'));
            // var uids_logout = JSON.parse(localStorage.getItem('uids_out'));

            $(document).ready(function() {

                if (faculty.length > 0) {
                    populateTable();
                    startTime();
                    initFaceDetectionControls();
                    loadDetectors();
                } else {

                    $('#alert').html('<div class="alert alert-warning"><strong>Oops!</strong> There are no faculty available at the moment.</div>');
                }
            });

            async function onPlay() {
                
                const videoEl = $('#inputVideo').get(0);

                if(videoEl.paused || videoEl.ended || !isFaceDetectionModelLoaded())
                    return setTimeout(() => onPlay());

                const ts = Date.now();

                const canvas = $('#overlay').get(0);
                const options = new faceapi.SsdMobilenetv1Options({ maxResults: 1,minConfidence: 0.9 });
                const fullFaceDescriptions = await faceapi.detectAllFaces(videoEl, options).withFaceLandmarks().withFaceDescriptors();
                const results = fullFaceDescriptions.map(fd => faceMatcher.findBestMatch(fd.descriptor));

                const dims = faceapi.matchDimensions(canvas, videoEl, true);
                const finalresult = faceapi.resizeResults(results, dims);
                
                updateTimeStats(Date.now() - ts);

                stored = JSON.parse(localStorage.getItem(storedLabel));

                finalresult.forEach((bestMatch, i) => {

                    const text = bestMatch.toString();
                    const uname = bestMatch._label.replace(/[0-9]/g, '');
                    const uid = bestMatch._label.replace(/\D/g, "");

                    if ( bestMatch._label != 'unknown' ) {

                        full_name = uname;

                        var userid = parseInt(uid);

                        var obj = stored.find(x => x.uid === userid);

                        // if record is stored
                        if( stored.findIndex(x => x.uid === userid) >= 0 ) 
                        {
                            var minutesToAdd=2;
                            var currentDate = new Date(obj.created_at);
                            var futureDate = new Date(currentDate.getTime() + minutesToAdd*60000);
                            var theday = new Date();
                            
                            if ( obj.login ) {
                                if (futureDate <= theday) {
                                    if( logout.indexOf(userid) < 0 ) {
                                        logout.push(userid);
                                    }
                                }
                            } 

                            if ( obj.logout ) {
                                if (futureDate <= theday) {
                                    if( logins.indexOf(userid) < 0 ) {
                                        logins.push(userid);
                                    }
                                }
                            }

                        } else {

                            if( logins.indexOf(userid) < 0 ) {
                                logins.push(userid);
                            }
                        }

                    } else {

                        full_name = 'Unrecognized';
                    }

                    const drawBox = new faceapi.draw.DrawBox(fullFaceDescriptions[i].detection.box, { label: full_name });
                    drawBox.draw($('#overlay').get(0));
                });

                setTimeout(() => onPlay());
            }
            
            function updateTimeStats(timeInMs) {
                forwardTimes = [timeInMs].concat(forwardTimes).slice(0, 30)
                const avgTimeInMs = forwardTimes.reduce((total, t) => total + t) / forwardTimes.length
                $('#time').text(`${Math.round(avgTimeInMs)} ms`)
                $('#fps').text(`${faceapi.utils.round(1000 / avgTimeInMs)}`)
            }

            async function run() 
            {
                $('.loading').hide();
                // try to access users webcam and stream the images
                // to the video element
                const stream = await navigator.mediaDevices.getUserMedia({ video: {} });
                const videoEl = $('#inputVideo').get(0);
                videoEl.srcObject = stream;
            }

            async function loadDetectors()
            {
                // await faceapi.loadMtcnnModel('/');
                await faceapi.loadSsdMobilenetv1Model('/');
                await faceapi.loadFaceLandmarkModel('/');
                await faceapi.loadFaceRecognitionModel('/');
                await loadUserImages();
            }

            async function detectFace(obj,image)
            {
                if (image) 
                {
                    const img = await faceapi.fetchImage('../uploads/images/'+image);
                    // detect the face with the highest score in the image and compute it's landmarks and face descriptor
                    // const fullFaceDescription = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor();
                    const fullFaceDescription = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor();
                    
                    if (!fullFaceDescription) {
                        
                        console.log(`no faces detected for ${obj.first_name} on image: ${image}`);

                    } else {
                        
                        // console.log('*****************************************');
                        // console.log(obj.first_name+' '+obj.last_name+obj.id);
                        // console.log('/uploads/images/'+image);
                        // console.log('*****************************************');
                        labeledDescriptors.push(
                            new faceapi.LabeledFaceDescriptors(
                                obj.first_name+' '+obj.last_name+obj.id,
                                [fullFaceDescription.descriptor]
                            )
                        );
                    }
                    
                }
            }

            async function loadUserImages()
            {
                if (faculty.length == 0 ) { return; }

                for ( var key in faculty ) 
                {
                    if ( faculty.hasOwnProperty( key ) ) 
                    {
                        if( faculty[key].profile_image ){ await detectFace(faculty[key], faculty[key].profile_image); }
                        // if( faculty[key].face_up ){ await detectFace(faculty[key], faculty[key].face_up); }
                        // if( faculty[key].face_down ){ await detectFace(faculty[key], faculty[key].face_down); }
                        // if( faculty[key].face_left ){ await detectFace(faculty[key], faculty[key].face_left); }
                        // if( faculty[key].face_right ){ await detectFace(faculty[key], faculty[key].face_right); }
                    }
                }

                const maxDescriptorDistance = 0.5;
                faceMatcher = new faceapi.FaceMatcher(labeledDescriptors, maxDescriptorDistance);

                await changeFaceDetector(SSD_MOBILENETV1);
                changeInputSize(128);

                await run();
            }

            function logUser() {

                logins = logins.filter((v, i, a) => a.indexOf(v) === i);
                logout = logout.filter((v, i, a) => a.indexOf(v) === i);

                if (logins.length > 0) 
                {
                    for (var i = 0; i < logins.length; i++)
                    {
                        var inuid = parseInt(logins[i]);

                        runAjax.push(
                            $.ajax({
                                url: '{{ route('log') }}',
                                type: 'get',
                                dataType: 'json',
                                data: {id: inuid, time:'in'},
                            }).always(function(e) {
                                // console.log(e.message);
                                if (e.error == false) 
                                {
                                    //localStorage.setItem('uids_in', JSON.stringify(e.uids_in));
                                    localStorage.setItem(storedLabel, JSON.stringify(e.records));

                                    var index = logins.indexOf(inuid);
                                    if (index > -1) {
                                        logins.splice(index, 1);
                                    }
                                }
                            })
                        );
                    }
                }

                if (logout.length > 0) 
                {
                    for (var i = 0; i < logout.length; i++)
                    {

                        var outuid = parseInt(logout[i]);

                        runAjax.push(
                            $.ajax({
                                url: '{{ route('log') }}',
                                type: 'get',
                                dataType: 'json',
                                data: {id: outuid, time:'out'},
                            }).always(function(e) {

                                // console.log(e.message);
                                if (e.error == false) 
                                {
                                    //localStorage.setItem('uids_out', JSON.stringify(e.uids_out));
                                    localStorage.setItem(storedLabel, JSON.stringify(e.records));

                                    var index = logout.indexOf(outuid);
                                    if (index > -1) {
                                        logout.splice(index, 1);
                                    }
                                }
                            })
                        );
                    }
                }

                if (runAjax.length > 0) 
                {
                    $.when.apply(null, runAjax).done(function(e){
                        populateTable();
                        logins = []; logout = [];
                        // listener = setInterval(logUser, 2000);
                    });
                }
            }

            function stopLogs(){
                clearInterval(listener);
            }

            function populateTable()
            {
                $('#loggedin tbody').html('');
                var loggedin = JSON.parse(localStorage.getItem(storedLabel));
                var html = '';

                if (loggedin.length) 
                {
                    for (var i = 0; i < loggedin.length; i++) 
                    {
                        var inout = (loggedin[i].login) ? 'in':'out';
                        var badge = (loggedin[i].login) ? 'success':'danger';

                        html += '<tr>';
                        // html += '<th scope="row">'+(parseFloat(i)+1)+'</th>';
                        html += '<td style="width:50px;"><div style="width:50px;height:50px;background:url(../uploads/images/'+loggedin[i].image+') no-repeat center center;background-size:cover;"></td>';
                        html += '<td>'+loggedin[i].first_name+' '+loggedin[i].middle_name+' '+loggedin[i].last_name+'</td>';
                        html += '<td><span class="badge badge-'+badge+'">Timed '+inout+'</span></td>';
                        html += '<td>'+loggedin[i].time_in+'</td>';
                        html += '</tr>';

                    }
                }

                $('#loggedin tbody').html(html);
            }

            function startTime() {
                jQuery('.time').html( currentTime() );
                setTimeout(startTime, 1000);
            }

            function currentTime()
            {
                var date = new Date();
                var hours = date.getHours();
                var minutes = date.getMinutes();
                var seconds = date.getSeconds();
                var ampm = hours >= 12 ? 'PM' : 'AM';
                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                return hours + ':' + checkTime(minutes) + ':'+checkTime(seconds)+' ' + ampm;
            }

            function checkTime(i) {
                if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
                return i;
            }

          </script>
    </body>
</html>