@extends('layouts.neon')

@section('title')
<div class="pull-left">
    <h2>{{ $user->first_name.' '.$user->middle_name.' '.$user->last_name }}</h2>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <a class="btn btn-primary btn-icon icon-left" href="{{ route('users.index') }}">Go Back <i class="entypo-left-open"></i></a>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-2">
        <div class="panel panel-default panel-shadow" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">Details </div>
                <div class="panel-options"></div>
            </div>
            <div class="panel-body">
                @if($user->image)
                    <div class="profile-picture">
                        <img src="{{ asset('/uploads/images/'.$user->image) }}" class="img-responsive img-circle ">
                    </div>
                @endif
                <hr>
                <strong>{{ $user->first_name .' '.$user->middle_name.' '. $user->last_name }}</strong>
                <br>
                {{ $user->email }}
                <hr>
                <div class="text-center">
                <a href="{{ route('users.edit',$user->id) }}" class="btn btn-default btn-icon">Edit <i class="entypo-pencil"></i></a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-10">

        <ul class="nav nav-tabs bordered" style="margin-top:0;">
            <li class="active">
                <a href="#daily-logs" data-toggle="tab">
                    <span class="visible-xs"><i class="entypo-home"></i></span>
                    <span class="hidden-xs">Time Logs</span>
                </a>
            </li>
            <li>
                <a href="#leaves" data-toggle="tab">
                    <span class="visible-xs"><i class="entypo-calendar"></i></span>
                    <span class="hidden-xs">Vacation Leave / Sick Leave</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="daily-logs">

                

                <div class="row">

                    <div class="col-md-4 col-sm-4 col-xs-12">

                        <form action="{{ route('users.show',$user->id) }}" method="GET">
                            <div class="input-group"> 
                                <select name="month" class="form-control">
                                    @foreach($months as $m => $nm )
                                    <option value="{{ $m }}"{{ ($m==$month) ? ' selected':'' }}>{{ $nm }}</option>
                                    @endforeach
                                </select>
                                <span class="input-group-btn"> 
                                    <button class="btn btn-primary btn-icon" type="submit">Records <i class="entypo-info"></i></button> 
                                </span> 
                            </div>
                        </form>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12"></div>



                    <div class="col-md-4 col-sm-4 col-xs-12">
                        @if( $month )
                        <a href="#" onclick="PrintElem('printme')" class="btn btn-info btn-icon btn-sm pull-right" style="margin-left:15px;">Print <i class="entypo-print"></i></a>
                        @endif
                        <a href="{{ route('users.show',$user->id) }}" class="btn btn-white btn-sm pull-right">Clear Filter</a>
                    </div>

                </div>

                <hr>

                @if( $month )

                    @if( count( $days ) > 0 )

                        <div id="printme">
                            <div style="text-align:center;">
                                <h3>DAILY TIME RECORD</h3>
                                <h4>{{ $user->first_name .' '.$user->middle_name.' '. $user->last_name }}</h4>
                                <h5>For the month of {{ $months[$month].' 1-'.$numdays.' '.date('Y') }}</h5>
                            </div>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th style="border-right-color: #f5f5f6;">AM</th>
                                        <th style="border-right-color: #f5f5f6;"></th>
                                        <th></th>
                                        <th style="border-right-color: #f5f5f6;">PM</th>
                                        <th style="border-right-color: #f5f5f6;"></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th>DATE</th>
                                        <th>IN</th>
                                        <th>OUT</th>
                                        <th>Undertime</th>
                                        <th>IN</th>
                                        <th>OUT</th>
                                        <th>Undertime</th>
                                        <th>Total Undertime</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php 
                                    $hours = 0; 
                                    $minutes = 0; 
                                    $seconds = 0; 
                                    $rate = (double) $user->rate;
                                    ?>

                                    @foreach( $days as $day => $d )

                                    <?php 

                                    $theday = explode(" ", $day);
                                    $satsun = strtolower($theday[1]);

                                    $amh = (empty($d['am']['undertime']['hours'])) ? '': $d['am']['undertime']['hours']; 
                                    $amm = (empty($d['am']['undertime']['minutes'])) ? '': $d['am']['undertime']['minutes']; 
                                    $ams = (empty($d['am']['undertime']['seconds'])) ? '': $d['am']['undertime']['seconds'];

                                    $pmh = (empty($d['pm']['undertime']['hours'])) ? '': $d['pm']['undertime']['hours']; 
                                    $pmm = (empty($d['pm']['undertime']['minutes'])) ? '': $d['pm']['undertime']['minutes']; 
                                    $pms = (empty($d['pm']['undertime']['seconds'])) ? '': $d['pm']['undertime']['seconds'];

                                    $unth = 0;
                                    $thours = 0; 

                                    if(!in_array($satsun, ['sat','sun'])) 
                                    {  
                                        $amh = preg_replace('/\D/', '', $amh);
                                        if( !empty($amh) ) { 
                                            $hours = $hours+$amh;
                                            $thours = $thours+$amh;
                                            $unth = $amh; $amh = $amh.':'; 
                                        } 
                                        
                                        $pmh = preg_replace('/\D/', '', $pmh);
                                        if( !empty($pmh) ) { 
                                            $hours = $hours+$pmh;
                                            $thours = $thours+$pmh;
                                            $unth = $unth+$pmh;$pmh = $pmh.':';  
                                        }

                                        $untm = 0;
                                        
                                        $amm = preg_replace('/\D/', '', $amm);
                                        if( !empty($amm) ) { 

                                            $minutes = $minutes+$amm;
                                            
                                            if( $minutes >= 60 ){
                                                $hours = $hours+1;
                                                $thours = $thours+1;
                                                $minutes = $minutes-60;
                                            }

                                            $untm = $amm; $amm = $amm.':'; 
                                        } 
                                        
                                        $pmm = preg_replace('/\D/', '', $pmm);
                                        if( !empty($pmm) ) { 

                                            $minutes = $minutes+$pmm;
                                            
                                            if( $minutes >= 60 ){
                                                $hours = $hours+1;
                                                $thours = $thours+1;
                                                $minutes = $minutes-60;
                                            }

                                            $untm = $untm+$pmm;$pmm = $pmm.':';  
                                        }

                                        $unts = 0;
                                        
                                        $ams = preg_replace('/\D/', '', $ams);
                                        if( !empty($ams) ) { 

                                            $seconds = $seconds+$ams;
                                            
                                            if( $seconds >= 60 ){
                                                $minutes = $minutes+1;
                                                $seconds = $seconds-60;
                                            }

                                            if( $minutes >= 60 ){
                                                $hours = $hours+1;
                                                $thours = $thours+1;
                                                $minutes = $minutes-60;
                                            }

                                            $unts = $ams; $ams = $ams; 
                                        } 
                                        
                                        $pms = preg_replace('/\D/', '', $pms);

                                        if( !empty($pms) ) { 

                                            $seconds = $seconds+$pms;
                                            
                                            if( $seconds >= 60 ){
                                                $minutes = $minutes+1;
                                                $seconds = $seconds-60;
                                            }

                                            if( $minutes >= 60 ){
                                                $hours = $hours+1;
                                                $thours = $thours+1;
                                                $minutes = $minutes-60;
                                            }

                                            $unts = $unts+$pms;
                                            $pms = $pms;
                                        }

                                        if ($unts) { 
                                            
                                            if ( $unts > 60 ) {
                                                $unts = $unts-60;
                                                $untm = $untm+1;
                                            }

                                            $unts = $unts; 

                                        } else {$unts = '';}

                                        if ($untm) { 
                                            
                                            if ( $untm > 60 ) {
                                                $untm = $untm-60;
                                                $unth = $unth+1;
                                            }

                                            $untm = $untm.':'; 

                                        } else {$untm = '';}

                                        if ($unth) { 
                                            
                                            $wage = 8-$unth;

                                            if($wage < 1){ 
                                                $wage = '0.00'; 
                                            } else {
                                                $wage = number_format(($wage*$rate), 2,'.',',');
                                            }

                                            $unth = $unth.':'; 

                                        } else {$unth = '';}
                                    }

                                    ?>

                                    <tr>
                                        <td>{{ $day }}</td>
                                        <td>{{ $d['am']['in'] }}</td>
                                        <td>{{ $d['am']['out'] }}</td>
                                        <td>{{ $amh.$amm.$ams }}</td>
                                        <td>{{ $d['pm']['in'] }}</td>
                                        <td>{{ $d['pm']['out'] }}</td>
                                        <td>{{ $pmh.$pmm.$pms }}</td>
                                        <td>
                                            @if(!in_array($satsun, ['sat','sun']))
                                            {{ $unth.$untm.$unts }}
                                            @endif
                                        </td>
                                        <td>
                                            @if(!in_array($satsun, ['sat','sun']))
                                            ₱ {{ ($wage) ? $wage : '0.00' }}
                                            @endif
                                        </td>
                                    </tr>

                                @endforeach

                                <tr class="info">
                                    <td><b>TOTAL WORKED HOURS</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><b>{{ $hours.':'.$minutes.':'.$seconds }} x ₱{{ $rate }}/hr = </td>
                                    <td><b>₱ {{ number_format(($hours*$rate), 2,'.',',') }}</b></td>
                                </tr>

                                </tbody>
                            </table>

                        </div>

                    @else

                    <div class="alert alert-warning"><strong>Oops!</strong> there are no available records for this date. User is falgged as <b>absent</b>.</div>

                    @endif

                @else

                    @if( $logs->count() )

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Date</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($logs as $log)
                            <tr>
                                <td>
                                    @if($log->login)
                                    <span class="btn btn-success btn-xs">
                                        <i class="entypo-check"></i> Time in
                                    </span> 
                                    @endif
                                    @if($log->logout)
                                    <span class="btn btn-info btn-xs">
                                        <i class="entypo-check"></i> Time out
                                    </span> 
                                    @endif
                                </td>
                                <td>{{ $log->created_at->format('F, j Y') }}</td>
                                <td>{{ $log->created_at->format('h:i:s A') }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $logs->render() !!}

                    @else

                    <div class="alert alert-warning"><strong>Oops!</strong> there are no available records for this date. User is falgged as <b>absent</b>.</div>
                    @endif

                @endif

            </div>
            <div class="tab-pane" id="leaves">
                

                <div class="row">
                    <div class="col-md-8">

                        @if( $leaves->count() )

                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>DATE</th>
                                    <th>TYPE</th>
                                    <th>OPTIONS</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($leaves as $leave)
                                <tr>
                                    <td>{{ $leave->created_at->format('F, j Y') }}</td>
                                    <td>
                                        @if($leave->cause == 'other')
                                        <span data-toggle="tooltip" data-placement="top" title="{{ $leave->reason }}">
                                            {{ ucwords( str_replace('-', ' ', $leave->cause) ) }}
                                        </span>
                                        @else
                                            {{ ucwords( str_replace('-', ' ', $leave->cause) ) }}
                                        @endif
                                    </td>
                                    <td>
                                        <form action="{{ route('leave-delete',$leave->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                                            <button class="btn btn-danger btn-xs" type="submit">Remove</button> 
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {!! $leaves->render() !!}

                        @else

                        <div class="alert alert-warning"><strong>Oops!</strong> there are no available records for this date.</div>
                        @endif

                        
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Register a leave</h3>
                            </div>
                            <div class="panel-body">

                                <form action="{{ route('leave',$user->id) }}" method="POST">
                                    @csrf
                                    @method('POST')
                                    <label for="">From:</label>
                                    <input type="date" name="from" class="form-control"><br>
                                    <label for="">To:</label>
                                    <input type="date" name="to" class="form-control"><br>
                                    <label for="">Type:</label>
                                    <select name="cause" class="form-control">
                                        <option value="sick-leave">Sick Leave</option>
                                        <option value="vacation-leave">Vacation Leave</option>
                                        <option value="other">Other</option>
                                    </select><br>
                                    <div class="reason">
                                        <label for="">Reason:</label>
                                        <textarea name="reason" rows="5" class="form-control" placeholder="State your reason..."></textarea>
                                        <br>
                                    </div>
                                    <button class="btn btn-success btn-icon" type="submit">Regester Leave <i class="entypo-check"></i></button> 
                                </form>
                            </div>
                        </div>

                        
                    </div>
                </div>

            </div>
        </div>


             
    </div>
</div>

@endsection
@section('header')
<style>
.reason{display: none;}
</style>
@endsection
@section('footer')
<script>
var $ = jQuery;
jQuery(document).ready(function(){
    $('[name="cause"]').change(function (e) {
        var c = $(this).val();
        if (c=='other') {
            $('.reason').fadeIn();
        } else {
            $('.reason').fadeOut();
        }
    });
});

function PrintElem(elem)
{
var mywindow = window.open('', 'PRINT', 'height=800,width=1000');

mywindow.document.write('<html><head><title>' + document.title  + '</title>');
mywindow.document.write('<style>table{width:100%;}table,td,th{ border:1px solid #ccc; }</style>');
mywindow.document.write('</head><body >');
mywindow.document.write(document.getElementById(elem).innerHTML);
mywindow.document.write('</body></html>');

mywindow.document.close(); // necessary for IE >= 10
mywindow.focus(); // necessary for IE >= 10*/

mywindow.print();
mywindow.close();

return true;
}
</script>
@endsection