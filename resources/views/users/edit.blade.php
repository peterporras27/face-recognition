@extends('layouts.neon')

@section('title')
<div class="pull-left">
    <h2>Edit User</h2>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <a class="btn btn-default" href="{{ route('users.index') }}"><i class="entypo-left-bold"></i> Go Back</a>
    </div>
</div>

<hr>
@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

{!! Form::model($user, ['method' => 'PATCH','enctype'=>'multipart/form-data','route' => ['users.update', $user->id]]) !!}


<ul class="nav nav-tabs bordered">
    <li class="active">
        <a href="#details" data-toggle="tab">
            <span class="visible-xs"><i class="entypo-home"></i></span>
            <span class="hidden-xs">User Details</span>
        </a>
    </li>
    <li>
        <a href="#face-recognition" data-toggle="tab">
            <span class="visible-xs"><i class="entypo-mail"></i></span>
            <span class="hidden-xs">Face Recognition</span>
        </a>
    </li>
    
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="details">
        <br>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <strong>First Name:</strong>
                    {!! Form::text('first_name', null, array('placeholder' => 'First Name','class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    <strong>Last Name:</strong>
                    {!! Form::text('last_name', null, array('placeholder' => 'Last Name','class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    <strong>Middle Name:</strong>
                    {!! Form::text('middle_name', null, array('placeholder' => 'Middle Name','class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    <strong>Hourly Rate:</strong>
                    <small><i>(Deafult is 140.00)</i></small>
                    {!! Form::number('rate', null, array('placeholder' => '140.00', 'class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    <strong>Department:</strong>
                    <select name="department_id" class="form-control">
                        @foreach($departments as $department)
                        <option value="{{ $department->id }}"{{ ($user->department_id==$department->id) ? ' selected':'' }}>{{ $department->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <strong>Role:</strong>
                    {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    <strong>Email:</strong>
                    {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    <strong>Password:</strong>
                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    <strong>Confirm Password:</strong>
                    {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="face-recognition">
        <br>
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="panel panel-default panel-shadow" data-collapsed="0">
                    <div class="panel-heading">
                        <div class="panel-title">Face looking straight</div>
                        <div class="panel-options">
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if($user->profile_image)
                            <img src="{{ asset('/uploads/images/'.$user->profile_image) }}" class="img-responsive img-circle">
                        @endif
                        <div class="form-group">
                            <input type="file" name="profile_image" class="form-control" accept="image/*" />
                        </div>
                    </div> 
                </div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default panel-shadow" data-collapsed="0">
                            <div class="panel-heading">
                                <div class="panel-title">Face looking Left</div>
                                <div class="panel-options">
                                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                @if($user->face_left)
                                    <img src="{{ asset('/uploads/images/'.$user->face_left) }}" class="img-responsive img-circle">
                                @endif
                                <div class="form-group">
                                    <input type="file" name="face_left" class="form-control" accept="image/*" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default panel-shadow" data-collapsed="0">
                            <div class="panel-heading">
                                <div class="panel-title">Face looking Up</div>
                                <div class="panel-options">
                                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                @if($user->face_up)
                                    <img src="{{ asset('/uploads/images/'.$user->face_up) }}" class="img-responsive img-circle">
                                @endif
                                <div class="form-group">
                                    <input type="file" name="face_up" class="form-control" accept="image/*" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default panel-shadow" data-collapsed="0">
                            <div class="panel-heading">
                                <div class="panel-title">Face leaning left</div>
                                <div class="panel-options">
                                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                @if($user->lean_left)
                                    <img src="{{ asset('/uploads/images/'.$user->lean_left) }}" class="img-responsive img-circle">
                                @endif
                                <div class="form-group">
                                    <input type="file" name="lean_left" class="form-control" accept="image/*" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default panel-shadow" data-collapsed="0">
                            <div class="panel-heading">
                                <div class="panel-title">Face looking Right</div>
                                <div class="panel-options">
                                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                @if($user->face_right)
                                    <img src="{{ asset('/uploads/images/'.$user->face_right) }}" class="img-responsive img-circle">
                                @endif
                                <div class="form-group">
                                    <input type="file" name="face_right" class="form-control" accept="image/*" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default panel-shadow" data-collapsed="0">
                            <div class="panel-heading">
                                <div class="panel-title">Face looking Down</div>
                                <div class="panel-options">
                                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                @if($user->face_down)
                                    <img src="{{ asset('/uploads/images/'.$user->face_down) }}" class="img-responsive img-circle">
                                @endif
                                <div class="form-group">
                                    <input type="file" name="face_down" class="form-control" accept="image/*" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default panel-shadow" data-collapsed="0">
                            <div class="panel-heading">
                                <div class="panel-title">Face leaning right</div>
                                <div class="panel-options">
                                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                @if($user->lean_right)
                                    <img src="{{ asset('/uploads/images/'.$user->lean_right) }}" class="img-responsive img-circle">
                                @endif
                                <div class="form-group">
                                    <input type="file" name="lean_right" class="form-control" accept="image/*" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>

<button type="submit" class="btn btn-primary btn-lg btn-icon">Save <i class="entypo-floppy"></i></button>

{!! Form::close() !!}


@endsection